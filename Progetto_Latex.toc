\select@language {italian}
\select@language {italian}
\contentsline {section}{\numberline {1}Documento di vision}{1}
\contentsline {subsection}{\numberline {1.1}Introduzione}{1}
\contentsline {subsection}{\numberline {1.2}Background}{2}
\contentsline {subsection}{\numberline {1.3}Requisiti generali e funzionalit\IeC {\`a}}{6}
\contentsline {subsection}{\numberline {1.4}Requisiti architetturali}{7}
\contentsline {section}{\numberline {2}Progettazione Possibili Soluzioni}{8}
\contentsline {subsection}{\numberline {2.1}Descrizione}{8}
\contentsline {subsection}{\numberline {2.2}Diagrammi BOAT}{8}
\contentsline {subsection}{\numberline {2.3}Architettura Party Level}{10}
\contentsline {subsection}{\numberline {2.4}Architettura System Level}{11}
\contentsline {subsection}{\numberline {2.5}Requisiti non funzionali}{12}
\contentsline {subsection}{\numberline {2.6}Architettura di massima}{13}
\contentsline {subsection}{\numberline {2.7}Diagramma di Gantt}{13}
\contentsline {section}{\numberline {3}Progettazione}{15}
\contentsline {subsection}{\numberline {3.1}Introduzione}{15}
\contentsline {subsection}{\numberline {3.2}Descrizione sintetica Use Case}{15}
\contentsline {subsection}{\numberline {3.3}Diagrammi Use Case}{15}
\contentsline {subsection}{\numberline {3.4}Descrizione dettagliata degli Use Case}{18}
\contentsline {subsection}{\numberline {3.5}Diagrammi BPMN}{24}
\contentsline {subsection}{\numberline {3.6}Diagramma delle classi}{26}
\contentsline {subsection}{\numberline {3.7}User eXperience}{27}
\contentsline {subsection}{\numberline {3.8}Modello BCE}{28}
